const error = (e) => console.log(e.target.responseText);
const nombre = document.getElementById('nombre').value;

/**
 * Método para guardar el usuario en la base de datos
 */
async function saveUser() {
    try {
        const ajaxRequest = new XMLHttpRequest();
        ajaxRequest.addEventListener("error", error);
        ajaxRequest.open("POST", "http://localhost:3000/api/users");
        ajaxRequest.setRequestHeader("Content-Type", "application/json");
        const data = {
            'nombre': document.getElementById('nombre').value,
            'apellidos': document.getElementById('apellidos').value,
            'email': document.getElementById('email').value,
            'contrasena': document.getElementById('contrasena').value,
            'direccion1': document.getElementById('direccion1').value,
            'direccion2': document.getElementById('direccion2').value,
            'pais': document.getElementById('pais').value,
            'ciudad': document.getElementById('ciudad').value,
            'postal': document.getElementById('postal').value,
            'telefono': document.getElementById('telefono').value,
            'perfil': "Usuario"
        };
        await ajaxRequest.send(JSON.stringify(data));
        alert("Usuario registrado")
        document.getElementById('nombre').value = "";
        document.getElementById('apellidos').value = "";
        document.getElementById('email').value = "";
        document.getElementById('contrasena').value = "";
        document.getElementById('direccion1').value = "";
        document.getElementById('direccion2').value = "";
        document.getElementById('pais').value = "";
        document.getElementById('ciudad').value = "";
        document.getElementById('postal').value = "";
        document.getElementById('telefono').value = "";
    } catch{
        alert("Error al guardar el usuario")
    }
}
