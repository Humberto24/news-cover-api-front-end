const error = (e) => console.log(e.target.responseText);
let url_string = window.location.href;
let url = new URL(url_string);
let idUsuario = url.searchParams.get("id");
let boton = document.getElementById("nombreBoton");
document.getElementById("noticias").href = `Noticias.html?id=${idUsuario}`
document.getElementById("categorias").href = `Categorias.html?id=${idUsuario}`
document.getElementById("dashboard").href = `Dashboard.html?id=${idUsuario}`
let id_fuente_noticia = "";

/**
 * Método para obtener el usuario logeado
 * @param {*} id 
 */
function getUser(id) {
    let url = "http://localhost:3000/api/users";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const userResponse = JSON.parse(response.target.responseText);
        console.log(userResponse)
        document.getElementById("nombreBoton").innerHTML = userResponse.nombre;
        if (userResponse.perfil === "Administrador") {
            document.getElementById("categorias").style.visibility = "visible";
        }
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

// Carga las categorias para meterlas al combobox
function get(id) {
    let url = "http://localhost:3000/api/categories";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const categoriesResponse = JSON.parse(response.target.responseText);
        console.log(categoriesResponse)
        let html;
        categoriesResponse.forEach(category => {
            html += `<option value="${category._id}">${category.categoria}</option>`
        })
        document.getElementById("idFuente").innerHTML = html;
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Guarda la fuente de noticias en la base de datos
 */
async function saveNewsSource() {
    try {
        const ajaxRequest = new XMLHttpRequest();
        ajaxRequest.addEventListener("error", error);
        ajaxRequest.open("POST", "http://localhost:3000/api/newsSources");
        ajaxRequest.setRequestHeader("Content-Type", "application/json");
        const data = {
            'url': document.getElementById('link').value,
            'nombre': document.getElementById('nombre').value,
            'id_categoria': document.getElementById('idFuente').value,
            'id_usuario': idUsuario
        };
        await ajaxRequest.send(JSON.stringify(data));
        window.location.href = `Noticias.html?id=${idUsuario}`;
    } catch (e) {
        console.log(e);
        alert("Error al guardar la fuente de noticia")
    }
}

getUser(idUsuario);
get();