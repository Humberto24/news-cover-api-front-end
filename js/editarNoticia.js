let url_string = window.location.href;
let url = new URL(url_string);
let idNuevo = url.searchParams.get("id");
let nombre = url.searchParams.get("name");
let link = url.searchParams.get("link");
let categoria = url.searchParams.get("categoria");
let idUsuario = url.searchParams.get("idUser");
let boton = document.getElementById("nombreBoton");
document.getElementById("noticias").href = `Noticias.html?id=${idUsuario}`
document.getElementById("categorias").href = `Categorias.html?id=${idUsuario}`
document.getElementById("dashboard").href = `Dashboard.html?id=${idUsuario}`
const error = (e) => console.log(e.target.responseText);

/**
 * Método para obtener el usuario logeado
 * @param {*} id 
 */
function getUser(id) {
    let url = "http://localhost:3000/api/users";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const userResponse = JSON.parse(response.target.responseText);
        console.log(userResponse)
        document.getElementById("nombreBoton").innerHTML = userResponse.nombre;
        if (userResponse.perfil === "Administrador") {
            document.getElementById("categorias").style.visibility = "visible";
        }
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Método para obtener las categorias y mostrarlas en el combobox
 * @param {*} id 
 */
function getCategories(id) {
    let url = "http://localhost:3000/api/categories";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const categoriesResponse = JSON.parse(response.target.responseText);
        console.log(categoriesResponse)
        let html;
        categoriesResponse.forEach(category => {
            if (category._id === categoria) {
                html += `<option value="${category._id}" selected>${category.categoria}</option>`
            }
            if(category._id !== categoria){
                html += `<option value="${category._id}">${category.categoria}</option>`
            }
        })
        document.getElementById("idFuente").innerHTML = html;
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Método para obtener las fuentes de noticias y mostrar la información en los inputs
 * @param {*} id 
 */
function get(id) {
    let url = "http://localhost:3000/api/newsSources";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const categoriesResponse = JSON.parse(response.target.responseText);
        document.getElementById('link').value = link
        document.getElementById('nombre').value = nombre
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Método para editar la fuente de noticia
 * @param {*} id 
 */
function editNews(id) {
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("PATCH", `http://localhost:3000/api/newsSources?id=${idNuevo}`);
    const data = {
        'url': document.getElementById('link').value,
        'nombre': document.getElementById('nombre').value,
        'id_categoria': document.getElementById('idFuente').value,
        'id_usuario': idUsuario
    };
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send(JSON.stringify(data));
    window.location.href = `Noticias.html?id=${idUsuario}`
}

getUser(idUsuario);
getCategories();
get(idNuevo);
