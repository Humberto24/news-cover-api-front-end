let url_string = window.location.href;
let url = new URL(url_string);
let idUsuario = url.searchParams.get("id");
let boton = document.getElementById("nombreBoton");
document.getElementById("noticias").href = `Noticias.html?id=${idUsuario}`
document.getElementById("categorias").href = `Categorias.html?id=${idUsuario}`
let cuadros = document.getElementById("cards");
const error = (e) => console.log(e.target.responseText);

/**
 * Método que verifica si el usuario tienes noticias
 * @param {} news 
 */
function NewsByUser(news) {
    var access = false;
    for (const item of news) {
        if (idUsuario === item.id_usuario) {
            access = true;
        }
    }
    relocate(access);
}

function relocate(access) {
    if (access === true) {
    } else {
        window.location.href = `Noticias.html?id=${idUsuario}`;
    }
}

/**
 * Método para obtener el usuario logeado
 * @param {} id 
 */
function getUser(id) {
    let url = "http://localhost:3000/api/users";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const userResponse = JSON.parse(response.target.responseText);
        document.getElementById("nombreBoton").innerHTML = userResponse.nombre;
        if (userResponse.perfil === "Administrador") {
            document.getElementById("categorias").style.visibility = "visible";
        }
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Metodo para obtener las categorias y pintarlas en la página
 * @param {} id 
 */
function getCategories(id) {
    let url = "http://localhost:3000/api/categories";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const categoriesResponse = JSON.parse(response.target.responseText);
        let html = "";
        categoriesResponse.forEach(item => {
            html += `<div class="card" style="width: 12rem;">
                        <ul class="list-group list-group-flush">
                            <button value="${item._id}" onclick="renderNewsByCategory('${item._id}')" class="list-group-item"
                            name="botonCategoria">${item.categoria}</button>
                        </ul>
                    </div>`;
        });
        html += `</div>`;
        document.getElementById("cuadritos").innerHTML = html;
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Método para renderizar las noticias filtrdas por una categoría
 */
function renderNewsByCategory(category) {
    let url = "http://localhost:3000/api/news";
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const news = JSON.parse(response.target.responseText);
        NewsByUser(news);
        let url = "http://localhost:3000/api/categories";
        const ajaxRequest1 = new XMLHttpRequest();
        ajaxRequest1.addEventListener("load", (response) => {
            const categories = JSON.parse(response.target.responseText);
            let html = `<div class="card-columns">`;
            news.forEach(element => {
                categories.forEach(categoryName => {
                    if (element.id_usuario === idUsuario) {
                        if (element.id_categoria === categoryName._id) {
                            if (element.id_categoria === category) {
                                html += `<div class="card mb-3">
                            <div class="card-body">
                                <p class="card-text">${element.fecha}</p>
                                <p class="card-text">${categoryName.categoria}</p>
                            </div> 
                            <div class="card-body">
                                <h5 class="card-title">${element.titulo}</h5>
                                <p class="card-text">${element.descripcion}</p>
                                <h6 class="card-title">
                                </h6>
                            </div>
                            <div class="card-footer">
                                <a href="${element.link}" class="card-link">Ver Noticia</a>
                            </div>
                            </div>
                            `;
                            } else if (category === 'Portada') {
                                html += `<div class="card mb-3">
                            <div class="card-body">
                                <p class="card-text">${element.fecha}</p>
                                <p class="card-text">${categoryName.categoria}</p>
                            </div> 
                            <div class="card-body">
                                <h5 class="card-title">${element.titulo}</h5>
                                <p class="card-text">${element.descripcion}</p>
                                <h6 class="card-title">
                                </h6>
                            </div>
                            <div class="card-footer">
                                <a href="${element.link}" class="card-link">Ver Noticia</a>
                            </div>
                            </div>
                        `;
                            }
                        }
                    }
                });
            });
            html += `</div>`;
            document.getElementById("cards").innerHTML = html
        })
        ajaxRequest1.addEventListener("error", error);
        ajaxRequest1.open("GET", url);
        ajaxRequest1.setRequestHeader("Content-Type", "application/json");
        ajaxRequest1.send();
    })
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

renderNewsByCategory('Portada')
getCategories()
getUser(idUsuario);