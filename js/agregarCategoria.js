const error = (e) => console.log(e.target.responseText);
let url_string = window.location.href;
let url = new URL(url_string);
let idUsuario = url.searchParams.get("id");
let boton = document.getElementById("nombreBoton");
document.getElementById("noticias").href = `Noticias.html?id=${idUsuario}`
document.getElementById("categorias").href = `Categorias.html?id=${idUsuario}`
document.getElementById("dashboard").href = `Dashboard.html?id=${idUsuario}`

/**
 * Método para obtener el usuario logeado
 * @param {*} id 
 */
function getUser(id) {
    let url = "http://localhost:3000/api/users";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const userResponse = JSON.parse(response.target.responseText);
        console.log(userResponse)
        document.getElementById("nombreBoton").innerHTML = userResponse.nombre;
        if (userResponse.perfil === "Administrador") {
            document.getElementById("categorias").style.visibility = "visible";
        }
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Método para guardar la categoria en la base de datos
 */
function saveCategory() {
    try {
        const ajaxRequest = new XMLHttpRequest();
        ajaxRequest.addEventListener("error", error);
        ajaxRequest.open("POST", "http://localhost:3000/api/categories");
        ajaxRequest.setRequestHeader("Content-Type", "application/json");
        const data = {
            'categoria': document.getElementById('categoria').value
        };
        ajaxRequest.send(JSON.stringify(data));
        window.location.href = `Categorias.html?id=${idUsuario}`
    } catch{
        alert("Error al guardar la categoria")
    }
}

getUser(idUsuario);
