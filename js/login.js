const error = (e) => console.log(e.target.responseText);

/**
 * Método para obtener los usarios de la base de datos, recorrerlos y comparar la información con los inputs para validar credenciales
 * @param {*} id 
 */
function get(id) {
    let url = "http://localhost:3000/api/users";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const userResponse = JSON.parse(response.target.responseText);
        userResponse.forEach(item => {
            if (document.getElementById('email').value === item.email 
                && document.getElementById("contrasena").value === item.contrasena) {
                window.location.href = `dashboard.html?id=${item._id}`;
            } else {
                
            }
        });
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}
