let url_string = window.location.href;
let url = new URL(url_string);
let idNuevo = url.searchParams.get("id");
let idUsuario = url.searchParams.get("idUser");
let boton = document.getElementById("nombreBoton");
document.getElementById("noticias").href = `Noticias.html?id=${idUsuario}`
document.getElementById("categorias").href = `Categorias.html?id=${idUsuario}`
document.getElementById("dashboard").href = `Dashboard.html?id=${idUsuario}`
const error = (e) => console.log(e.target.responseText);

/**
 * Método para obtener el usuario logeado
 * @param {*} id 
 */
function getUser(id) {
    let url = "http://localhost:3000/api/users";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const userResponse = JSON.parse(response.target.responseText);
        console.log(userResponse)
        document.getElementById("nombreBoton").innerHTML = userResponse.nombre;
        if (userResponse.perfil === "Administrador") {
            document.getElementById("categorias").style.visibility = "visible";
        }
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Método para obtener la categoria y mostrarla en el input
 * @param {*} id 
 */
function get(id) {
    let url = "http://localhost:3000/api/categories";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const categoriesResponse = JSON.parse(response.target.responseText);
        document.getElementById("categoria").value = categoriesResponse.categoria;
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Método para editar la categoria
 * @param {*} id 
 */
function editCategory(id) {
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("PATCH", `http://localhost:3000/api/categories?id=${idNuevo}`);
    const data = {
      "categoria": document.getElementById('categoria').value
    };
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send(JSON.stringify(data));
    window.location.href = `Categorias.html?id=${idUsuario}`
}

getUser(idUsuario);
get(idNuevo);
  