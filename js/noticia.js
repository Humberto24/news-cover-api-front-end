const error = (e) => console.log(e.target.responseText);
let url_string = window.location.href;
let url = new URL(url_string);
let idUsuario = url.searchParams.get("id");
document.getElementById("noticias").href = `Noticias.html?id=${idUsuario}`
document.getElementById("categorias").href = `Categorias.html?id=${idUsuario}`
document.getElementById("agregarNoticia").href = `AgregarNoticia.html?id=${idUsuario}`
document.getElementById("dashboard").href = `Dashboard.html?id=${idUsuario}`

/**
 * Método para obtener el usuario logeado
 * @param {} id 
 */
function getUser(id) {
    let url = "http://localhost:3000/api/users";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const userResponse = JSON.parse(response.target.responseText);
        document.getElementById("nombreBoton").innerHTML = userResponse.nombre;
        if (userResponse.perfil === "Administrador") {
            document.getElementById("categorias").style.visibility = "visible";
        }
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Metodo para obtener las fuentes de noticias recorrerlas e insertar las noticias del dashboard con el id de las fuentes de noticias
 * @param {*} id 
 */
function getNews(id) {
    let url = "http://localhost:3000/api/newsSources";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const newsResponse = JSON.parse(response.target.responseText);
        newsResponse.forEach(element => {
            fetch(`https://cors-anywhere.herokuapp.com/${element.url}`)
                .then((response) => {
                    response.text().then((xml) => {
                        let xmlContent = xml;
                        let parser = new DOMParser();
                        let xmlDOM = parser.parseFromString(xmlContent, 'application/xml')
                        let news = xmlDOM.querySelectorAll("item")
                        news.forEach((item, i) => {
                            if (news.length > 48) {
                                if (element.id_usuario === idUsuario) {
                                    let titulo = item.children[1].innerHTML;
                                    let tituloArreglado = titulo.slice(9, -3);
                                    let link = item.children[2].innerHTML;
                                    let linkArreglado = link.slice(9, -3);
                                    let fecha = item.children[3].innerHTML
                                    let descripcion = item.children[4].innerHTML;
                                    let descripcionArreglada = descripcion.slice(9, -3);
                                    try {
                                        const ajaxRequest = new XMLHttpRequest();
                                        ajaxRequest.addEventListener("error", error);
                                        ajaxRequest.open("POST", "http://localhost:3000/api/news");
                                        ajaxRequest.setRequestHeader("Content-Type", "application/json");
                                        const data = {
                                            'titulo': tituloArreglado,
                                            'descripcion': descripcionArreglada,
                                            'link': linkArreglado,
                                            'fecha': fecha,
                                            'id_fuente_noticia': element._id,
                                            'id_usuario': element.id_usuario,
                                            'id_categoria': element.id_categoria
                                        };
                                        ajaxRequest.send(JSON.stringify(data));
                                        i++;
                                        if (i >= 10) {
                                            return;
                                        }
                                    } catch (e) {
                                        console.log("Error al guardar la noticia", e)
                                    }
                                }
                            } else {
                                if (element.id_usuario === idUsuario) {
                                    try {
                                        const ajaxRequest = new XMLHttpRequest();
                                        ajaxRequest.addEventListener("error", error);
                                        ajaxRequest.open("POST", "http://localhost:3000/api/news");
                                        ajaxRequest.setRequestHeader("Content-Type", "application/json");
                                        const data = {
                                            'titulo': item.children[0].innerHTML,
                                            'descripcion': item.children[10].innerHTML,
                                            'link': item.children[1].innerHTML,
                                            'fecha': item.children[3].innerHTML,
                                            'id_fuente_noticia': element._id,
                                            'id_usuario': element.id_usuario,
                                            'id_categoria': element.id_categoria
                                        };
                                        ajaxRequest.send(JSON.stringify(data));
                                        i++;
                                        if (i >= 10) {
                                            return;
                                        }
                                    } catch (e) {
                                        console.log("Error al guardar la noticia", e)
                                    }
                                }
                            }
                        });
                    })
                });
        });
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Metodo para pintar las noticias 
 * @param {*} newsResponse 
 */
function renderNewsSources(newsSourceResponse) {
    let url = "http://localhost:3000/api/categories";
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const categories = JSON.parse(response.target.responseText);
        let html = `<table class="table">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Category</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>`;
        newsSourceResponse.forEach((newssource, i) => {
            categories.forEach(category => {
                if (newssource.id_usuario === idUsuario) {
                    if (newssource.id_categoria === category._id) {
                        html += `<thead class="thead-dark">
                    </thead>
                    <tbody>
                        <tr>
                        <td>${newssource.nombre}</td>
                        <td>${category.categoria}</td>   
                        <td>
                            <a href="EditarNoticia.html?id=${newssource._id}&name=${newssource.nombre}&link=${newssource.url}&categoria=${newssource.id_categoria}&idUser=${idUsuario} "><button type="button" class="btn btn-success">Edit</button></a>
                            <a href=Noticias.html?id=${idUsuario}> <button onclick="deleteNewsSource('${newssource._id}')" type="button" class="btn btn-danger">Delete</button></a>
                        </td>
                        </tr>
                     </tbody>`;
                    }
                }
            });
        });
        html += `</table>`
        document.getElementById("tableList").innerHTML = html
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Obtiene los datos de la fuente de noticias para pasarlos a la funcion que los renderiza
 * @param {*} id 
 */
function get(id) {
    let url = "http://localhost:3000/api/newsSources";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const newsResponse = JSON.parse(response.target.responseText);
        renderNewsSources(newsResponse);
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Elimina todas las noticias antes de volverlas a insertar
 */
function deleteAllNews() {
    let url = "http://localhost:3000/api/news";
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const newsResponse = JSON.parse(response.target.responseText);
        newsResponse.forEach(item => {
            if (item.id_usuario === idUsuario) {
                deleteNews(item._id)
            }
        });
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Metodo que elimina las fuentes de noticia
 */
function deleteNewsSource(id) {
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("DELETE", `http://localhost:3000/api/newsSources?id=${id}`);
    ajaxRequest.send();
}

/**
 * Método para eliminar una fuente de noticia por id
 * @param {*} id 
 */
function deleteNews(id) {
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("DELETE", `http://localhost:3000/api/news?id=${id}`);
    ajaxRequest.send();
}

getUser(idUsuario);
get();
deleteAllNews();
getNews();

