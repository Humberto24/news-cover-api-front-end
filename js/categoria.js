const error = (e) => console.log(e.target.responseText);
let url_string = window.location.href;
let url = new URL(url_string);
let idUsuario = url.searchParams.get("id");
let boton = document.getElementById("nombreBoton");
document.getElementById("noticias").href = `Noticias.html?id=${idUsuario}`
document.getElementById("categorias").href = `Categorias.html?id=${idUsuario}`
document.getElementById("agregarCategoria").href = `AgregarCategoria.html?id=${idUsuario}`
document.getElementById("dashboard").href = `Dashboard.html?id=${idUsuario}`

/**
 * Método para obtener el usuario logeado
 * @param {} id 
 */
function getUser(id) {
    let url = "http://localhost:3000/api/users";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const userResponse = JSON.parse(response.target.responseText);
        console.log(userResponse)
        document.getElementById("nombreBoton").innerHTML = userResponse.nombre;
        if (userResponse.perfil === "Administrador") {
            document.getElementById("categorias").style.visibility = "visible";
        }
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Método para obtener las categorias y pintarlas
 * @param {} id 
 */
function get(id) {
    let url = "http://localhost:3000/api/categories";
    if (id) {
        url = `${url}?id=${id}`;
    }
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("load", (response) => {
        const categoriesResponse = JSON.parse(response.target.responseText);
        let html = `<table class="table">
            <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Actions</th>
            </tr>`;
        categoriesResponse.forEach(category => {
            html += `<thead class="thead-dark">
            </thead>
            <tbody>
                <tr>
                    <td>${category.categoria}</td>
                    <td>
                        <a href="EditarCategoria.html?id=${category._id}&idUser=${idUsuario}"><button type="button" class="btn btn-success">Edit</button></a>
                        <a href=Categorias.html?id=${idUsuario}> <button onclick="deleteCategory('${category._id}')" type="button" class="btn btn-danger">Delete</button></a>
                    </td>
                </tr>
            </tbody>`;
        });
        html += '</table>';
        document.getElementById('tableList').innerHTML = html;
    });
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("GET", url);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();
}

/**
 * Método para eliminar una categoria por id
 * @param {} id 
 */
function deleteCategory(id) {
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("DELETE", `http://localhost:3000/api/categories?id=${id}`);
    ajaxRequest.send();
    get();
}

getUser(idUsuario);
get();
